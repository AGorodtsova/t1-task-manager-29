package ru.t1.gorodtsova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.service.IAuthService;
import ru.t1.gorodtsova.tm.api.service.IUserService;
import ru.t1.gorodtsova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
