package ru.t1.gorodtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Bind task to project";

    @NotNull
    private final String NAME = "task-bind-to-project";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(getUserId(), projectId, taskId);
    }

}
