package ru.t1.gorodtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.IRepository;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> records = new ArrayList<>();

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        records.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        records.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        removeAll();
        return add(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        return records
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void removeAll() {
        records.clear();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        return records.get(index);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final M model) {
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        removeOne(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable Collection<M> collection) {
        records.removeAll(collection);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Override
    public int getSize() {
        return records.size();
    }

}
