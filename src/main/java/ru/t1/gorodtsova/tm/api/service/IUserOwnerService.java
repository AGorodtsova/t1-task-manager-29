package ru.t1.gorodtsova.tm.api.service;

import ru.t1.gorodtsova.tm.api.repository.IUserOwnedRepository;
import ru.t1.gorodtsova.tm.model.AbstractUserOwnedModel;

public interface IUserOwnerService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {
}
